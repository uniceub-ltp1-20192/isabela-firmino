#include <stdio.h>
#include <stdlib.h>

int contaVogal(char [] );


int main(){
    char nome[15], sexo, simnao;
    int contador;

    printf("Digite o nome : \n");
    scanf("%s", nome);

    contador = contaVogal(nome);
    if(contador == 0){
        printf("SUSPEITO!\n");
        return 0;
    };

    printf("Qual o sexo? <F> ou <M>\n");    
    scanf("%c", &sexo); 
    if((sexo == 'f') || (sexo == 'F')){
        printf("SUSPEITA!\n");
        return 0;
    };

    printf("Ele estava no saguão do hotel às 00:30 ? <S> ou <N>\n");
    scanf("%c", &simnao);

    if((simnao == 's') || (simnao == 'S')){
        printf("SUSPEITO!\n");
        return 0;
    }; 
    

    return 0;

};

int contaVogal(char palavra[]){
    int i = 0, contador = 0;

    while( palavra[i] != '\0' && contador == 0){
        if((palavra[i] == 'O') || (palavra[i] == 'o') || (palavra[i] == 'U') || (palavra[i] == 'u')){
            contador++;
        }
        i++;        
    }

    return contador;

};