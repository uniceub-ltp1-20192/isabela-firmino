from Entidades.entidadeMae import Pessoa

class Nacao(Pessoa):
  def __init__(self, pais = "Brasil"):
    super().__init__()
    self._pais = pais

  @property
  def pais(self):
    return self._pais

  @pais.setter
  def pais(self,pais):
    self._pais = pais

  def fala(self):
    print("Oi eu sou do(a) ", self.pais)
  
  def __str__(self):
    return '''
    ---- Pessoa ---- 
    Identificador: {}
    Nome : {}
    Altura : {}
    Peso : {}
    País : {}
    '''.format(self.identificador,self.nome,self.altura,self.peso,self.pais)
