#12.4  Um  buffet  oferece   5   tipos   de   comida.   Pense   em   5   tipos   de
#comida e as guarde em um tupla.

buffet = ('sushi', 'galinhada', 'churrasco', 'vegetariana', 'frutas')

#• Use um loop for para imprimir cada comida que o restaurante
#oferece.

for x in range(0,len(buffet)):
	print(buffet[x])
	pass

#• Tente modificar um dos itens, e garanta que o python rejeita
#a alteração. 

buffet[1]='bolo'

#• O restaurante mudou o menu, trocando dois pratos do cardápio.
#Adicione   um   bloco   de   código   que   rescreve   a   tupla,   e   então
#reusa o loop for para imprimir os itens do novo menu. 
print("\n----------------------\n")
buffet = ('sushi', 'bolos', 'churrasco', 'vegetariana', 'salada')
for x in range(0,len(buffet)):
	print(buffet[x])
	pass