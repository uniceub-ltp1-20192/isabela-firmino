#7.1 Se você pudesse convidar alguém, vivo ou morto, para
#jantar, quem você convidaria? Faça uma lista que inclua
#pelo   menos   três   pessoas   que   você   gostaria   de   convidar
#para o jantar. Em seguida, use sua lista para imprimir
#uma mensagem para cada pessoa, convidando­a para jantar.

convidados = ['Benedict Cumberbatch', 'Robert Downey Jr', 'Chris Evans', 'Mark Ruffalo', 'Chris Hemsworth']
reset = 0
for x in range(1, 6):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

#7.2  Acabou de ouvir que um dos seus convidados não pode
#fazer   o   jantar,   por   isso   precisa   de   enviar   um   novo
#conjunto   de   convites.   Você   terá   que   pensar   em   outra
#pessoa para convidar.
#• Comece com o seu programa do exercício 7.1. 
#Adicione uma   declaração   de   impressão   no   final do programa,
#informando   o   nome   do   convidado   que   não   pode comparecer.
#• Modifique   sua   lista,   substituindo   o   nome   do
#convidado que não pode comparecer com o nome da nova
#pessoa que você está convidando.
#• Imprima um segundo conjunto de mensagens de convite,
#uma para cada pessoa que ainda esteja na sua lista.

from random import randrange, uniform
print("\n\n\n----------------------\n\n\n")
convidados = ['Benedict Cumberbatch', 'Robert Downey Jr', 'Chris Evans', 'Mark Ruffalo', 'Chris Hemsworth']
reset = 0
for x in range(1, 6):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

a = randrange(0, 5)
furao = convidados.pop(a)
print("\nO convidado", furao, "infeslismente não vai comparecer bjs\n")

reset = 0
for f in range(1, 5):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

#7.3  Você acabou de encontrar uma mesa de jantar maior,
#então agora há mais espaço disponível. Pense em mais três
#convidados para convidar para o jantar.
#• Comece com o seu programa no Exercício 7.2. Adicione
#uma   mensagem   de   impressão   ao   final   do   programa
#informando às pessoas que você encontrou uma mesa de
#jantar maior
#• Use   insert()   para   adicionar   um   novo   convidado   ao
#início de sua lista.
#• Use   insert   ()   para   adicionar   um   novo   convidado   ao
#meio da sua lista.
#• Use   append()   para   adicionar   um   novo   convidado   ao
#final da sua lista.
#• Imprima   um   novo   conjunto   de   mensagens   de   convite,
#uma para cada pessoa na sua lista.

from random import randrange, uniform
print("\n\n\n----------------------\n\n\n")
convidados = ['Benedict Cumberbatch', 'Robert Downey Jr', 'Chris Evans', 'Mark Ruffalo', 'Chris Hemsworth']
reset = 0
for x in range(1, 6):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

print("\n\n\n----------------------\n\n\n")

a = randrange(0, 5)
furao = convidados.pop(a)
print("\nO convidado", furao, "infeslismente não vai comparecer bjs\n")

reset = 0
for f in range(1, 5):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

print("\n\n\n----------------------\n\n\n")

print("Encontreiuma mesa maior amigos XD\n")


convidados.insert(0,'Scarlett Johansson')
convidados.insert(2, 'Jeremy Renner')
convidados.insert(8, 'Don Cheadle')

reset = 0
for f in range(1, 8):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

#7.4 Você acabou de descobrir que sua nova mesa de jantar
#não   chegará   a   tempo   para   o   jantar   e   terá   espaço   para
#apenas dois convidados.
#• Comece   com   seu   programa   do   Exercício   7.3.  Adicione
#uma nova linha que imprima uma mensagem dizendo que
#você   pode   convidar   apenas   duas   pessoas   para   o
#jantar.
#• Use  pop()  para remover os convidados da sua lista,
#um de cada vez, até que apenas dois nomes permaneçam
#na sua lista. Sempre que você inserir um nome na sua
#lista,   imprima   uma   mensagem   para   essa   pessoa,
#informando­o de que não pode convidá­lo para jantar.
#• Imprima uma mensagem para cada uma das duas pessoas
#que ainda estão na sua lista, informando­as de que
#ainda estão convidadas.
#• Use   del   para   remover   os   dois  últimos   nomes   da   sua
#lista, para que você tenha uma lista vazia. Imprima
#sua lista para ter certeza de que você realmente tem
#uma lista vazia no final do seu programa.

from random import randrange, uniform
print("\n\n\n----------------------\n\n\n")
convidados = ['Benedict Cumberbatch', 'Robert Downey Jr', 'Chris Evans', 'Mark Ruffalo', 'Chris Hemsworth']
reset = 0
for x in range(1, 6):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

print("\n\n\n----------------------\n\n\n")

a = randrange(0, 5)
furao = convidados.pop(a)
print("\nO convidado", furao, "infeslismente não vai comparecer bjs\n")

reset = 0
for f in range(1, 5):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

print("\n\n\n----------------------\n\n\n")

print("Encontrei uma mesa maior amigos XD\n")


convidados.insert(0,'Scarlett Johansson')
convidados.insert(2, 'Jeremy Renner')
convidados.insert(8, 'Don Cheadle')

reset = 0
for f in range(1, 8):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass

print("\n\n\n----------------------\n\n\n")

print("A nova mesa não fegará e portanto só poderei chamar apenas 2 convidados\n")

convidados.pop(1)
convidados.pop(1)
convidados.pop(1)
convidados.pop(1)
convidados.pop(1)

reset = 0
for f in range(1, 3):
	print("É com muita felicidade que eu convido", convidados[reset], "para jantar.\n")
	reset +=1
	pass



