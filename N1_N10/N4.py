#4.1  Armazene   o   nome   de   uma   pessoa   e   inclua   alguns
#caracteres de espaço em branco no início e no final do nome.
#Certifique­se   de   usar   cada   combinação   de
#caracteres, "\t" e "\n", pelo menos uma vez. 
#Imprima o nome uma vez, para que o espaço em branco ao redor do
#nome seja exibido. 

nome = str(input("Nome : "))
sobrenome = str(input("Sobrenome : "))
print("\n \t" ,nome, "\n\t", sobrenome, "\n")

#Em seguida, imprima o nome usando cada uma das três funções de  
#DECAPAGEM  (stripping)  lstrip(), rstrip() e strip().
print("------------------------\n")
print(nome.lstrip())
print(nome.rstrip())
print(nome.strip())

#4.2  Solicite   uma   frase   ao   usuário   e   apresente   quantos
#caracteres   existem   na   frase   sem   contar   os   espaços   em
#branco.

print("\n------------------------\n")
frase = str(input("Frase : "))
result = frase.replace(" ", "")
print(len(result))