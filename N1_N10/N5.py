#5.1  Crie um programa que solicite um valor ao usuário e
#informe se o mesmo é par ou impar.

print("--------------------------------\n")
numero = float(input("Número : "))
if (numero%2) == 0:
	print("par")
else:
	print("impar")
print("\n--------------------------------")

#5.2  Conceba uma programa que  informe se um  determinado
#número é primo.

print("--------------------------------\n")
primo = int(input("Número : "))
tot = 0
for c in range(1, primo + 1):
	if primo % c == 0:
		tot +=1

if tot == 2:
	print("primo")
else:
	print("não é primo")
print("\n--------------------------------")

#5.3 Crie uma calculadora automática que dado dois valores
#informe o resultado da adição, subtração, multiplicação e
#divisão.

print("--------------------------------\n")
valor_1 = float(input("Valor 1 : "))
valor_2 = float(input("Valor 2 : "))

adicao = valor_1 + valor_2
subtracao = valor_1 - valor_2
multiplicacao = valor_1 * valor_2
divisao = valor_1 / valor_2

print("\nAdição: ", adicao)
print("Subtração: ", subtracao)
print("Multiplicação: ", multiplicacao)
print("Divisão: ", divisao)
print("\n--------------------------------")

#5.4 Crie uma programa que fornecidos a idade e o semestre
#que   um   pessoa  está  informa   em  quantos  anos   ela   vai   se
#formar.

print("--------------------------------\n")
idade = int(input("Idade: "))
semestre = int(input("Semestre: "))

falta = 8 - semestre
anos = falta/2
print("Vai se formar em", anos, "anos")
print("\n--------------------------------")

#5.5 Crie uma variação do programa 5.4 que receba também a
#informação de quantos semestres o aluno está atrasado e
#indique qual o tempo total que ele vai ter no curso até
#se formar.

print("--------------------------------\n")
idade = int(input("Idade: "))
semestre = int(input("Semestre: "))
atrasado = int(input("Quantos semestres de atraso: "))

falta = 8 - (semestre - atrasado)
anos = falta/2
print("Vai se formar em", anos, "anos")
print("\n--------------------------------")







