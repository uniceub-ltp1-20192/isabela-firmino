#8.1  Pense em pelo menos cinco lugares no mundo que você gostaria de visitar.
#• Armazene   os   locais   em   uma   lista.   Certifique­se de que a lista 
#não esteja em ordem alfabética.

lugares = ['Grécia', 'Japão', 'Itália', 'Amsterdã', 'Hawaii']

#• Imprima   sua   lista   na   ordem   original.   Não   se preocupe   em  
#imprimir   a   lista   item   por   item, basta imprimi­la como uma lista bruta do Python.

print("\nOriginal:\n")
print(lugares)

#• Use  sorted()  para   imprimir   sua   lista   em   ordem alfabética sem modificar 
#a lista atual.

print("\nOrdem alfabética:\n")
print(sorted(lugares))

#• Mostre   que   sua   lista   ainda   está   em   sua   ordem original, imprimindo­a.

print("\nOriginal:\n")
print(lugares)

#• Use  sorted()  para   imprimir   sua   lista   em   ordem alfabética inversa sem 
#alterar a ordem da lista original.

print("\nOrdem alfabética inversa:\n")
print(sorted(lugares, reverse=True))

#• Mostre   que   sua   lista   ainda   está   em   sua   ordem original, imprimindo­a.

print("\nOriginal:\n")
print(lugares)

#• Use reverse() para alterar a ordem da sua lista.

lugares.reverse()

#Imprima   a   lista   para   mostrar   que   a   ordem   foi alterada.

print("\nAlterar a ordem da sua lista:\n")
print(lugares)

#• Use  reverse()  para alterar a ordem da sua lista novamente. 

lugares.reverse()

#Imprima a lista para mostrar que ela está de volta ao pedido original.

print("\nOriginal:\n")
print(lugares)

#• Use  sort()  para que a lista seja armazenada em ordem   alfabética.

lugares.sort()

#Imprima   a   lista   para   mostrar que a ordem foi alterada.

print("\nOrdem alfabética:\n")
print(lugares)

#• Use  sort()  para que a lista seja armazenada em ordem   alfabética   inversa.

lugares.sort(reverse=True)

#Imprima   a   lista   para mostrar que a ordem foi alterada.

print("\nOrdem alfabética inversa:\n")
print(lugares)
print("\n\n\n----------------------\n\n\n")

#8.2  Altere   os   códigos   desenvolvidos   para   o   convite   de
#jantar   (Notas   de   aula   7)   para   que   seja   apresentada   a
#quantidade de convidados existentes.

convidados = ['Benedict Cumberbatch', 'Robert Downey Jr', 'Chris Evans', 'Mark Ruffalo', 'Chris Hemsworth']
reset = 0
for x in range(1, 6):
	print("É com muita felicidade que eu convido", convidados[reset], "para um jantar com", len(convidados), "convidados.\n")
	reset +=1
	pass
print("\n\n\n----------------------\n\n\n")

#8.3 Todas as funções!    Pense em algo que você gostaria armazenar em uma lista. 
#Por exemplo, você pode criar uma lista   de   montanhas,   rios, países, cidades, 
#idiomas ou qualquer   outra   coisa   que deseje. Escreva um programa que crie 
#uma lista contendo esses   itens   e,   em   seguida, use cada função apresentada
#nesta   nota   pelo   menos   uma vez.

familia = ['Isabela', 'Daniella', 'Sinval', 'Maria Aparecida']
print(familia)
print("Eu sou a", familia[0])
print("Ainda tem minha tia Núbia e meu tio Junior")
familia.append("Núbia")
familia.insert(0, "Junior")
print(familia)
print("Tirando eu minha família fica:")
familia.pop(1)
print(familia)
print("Em ordem alfabética:")
print(sorted(familia))
print("Em ordem alfabética reversa:")
print(sorted(familia, reverse=True))


