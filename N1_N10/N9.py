#9.1  Implemente   um   código   em   Python   que   some   todos
#elementos dentro de uma lista.

elementos = []
quantos = int(input("Quantos elementos: "))
for x in range(1,quantos + 1):
	elementos.append(int(input("Número: ")))
	pass
print("Soma:",sum(elementos))

#9.2 Implemente um código em Python que multiplique todos
#elementos dentro de uma lista.

print("\n\n\n----------------------\n\n\n")
elementos = []
quantos = int(input("Quantos elementos: "))
for x in range(1,quantos + 1):
	elementos.append(int(input("Número: ")))
	pass
lenn = len(elementos)
mult = 1
for oi in range(0, lenn):
	mult *= elementos[oi] 
	pass
print("Multiplicação:",mult)

#9.3 Escreva   um   código   que   retorne   o   menor   elemento   de
#uma lista.

print("\n\n\n----------------------\n\n\n")
elementos = []
quantos = int(input("Quantos elementos: "))
for x in range(1,quantos + 1):
	elementos.append(int(input("Número: ")))
	pass
lenn = len(elementos)
print("Menor elemento:",min(elementos))


#9.4 Escreva um código que fornecidas duas listas imprima
#uma lista com os elementos comuns entre elas.
#Exemplo: ListaA = [“ABC”, “DEF”, “GHI”, “YWZ”]
#    ListaB = [“CEB”, “YWZ”, “ABC”]
#Saída: [“ABC”, “YWZ”]

print("\n\n\n----------------------\n\n\n")
lista_1 = []
lista_2 = []
elementos_1 = int(input("Quantos elementos na lista 1: "))
for x in range(1,elementos_1 + 1):
	lista_1.append((input("Input : ")))
	pass
elementos_2 = int(input("Quantos elementos na lista 2: "))
for y in range(1,elementos_2 + 1):
	lista_2.append((input("Input : ")))
	pass
lista_final = [a for a in lista_2 if a in lista_1]
print("Elementos em comum:", lista_final)

#9.5 Implemente um programa que imprima todos elementos de
#uma lista em ordem alfabética

print("\n\n\n----------------------\n\n\n")
elementos = []
quantos = int(input("Quantos elementos: "))
for x in range(1,quantos + 1):
	elementos.append(str(input("Nome: ")))
	pass

print("Ordem alfabética:",sorted(elementos))

#9.6  Implemente   um   código   que   imprima   uma   lista   com   o
#tamanho de cada elemento da lista de entrada.
#Exemplo: lista = [“cebola”, “carro”, “oi”, “mansões”]
#Saída: [6,5,2,7]

print("\n\n\n----------------------\n\n\n")
elementos = []
quantos = int(input("Quantos elementos: "))
for x in range(1,quantos + 1):
	elementos.append(str(input("Nome: ")))
	pass
lenn = len(elementos)
num = []
for oi in range(0, lenn):
	num.append(len(elementos[oi]))
	pass
print(num)

#9.7  Implemente   um   programa   que   fornecida   uma   lista   de
#palavras e uma palavra indique quais elementos da lista
#são anagramas da palavra. 

print("\n\n\n----------------------\n\n\n")
alternatives = []
elementos = int(input("Quantos elementos na lista: "))
for x in range(1,elementos + 1):
	alternatives.append((input("Input : ")))
	pass
word = sorted(input("Palavra: "))
for alt in alternatives:
    if word == sorted(alt):
        print(alt)




