#2.1 Crie um programa que defina duas variáveis, 
#quantidade_cebolas e preco_cebola, 
#atribua valores inteiros para essas variáveis. 
#Imprima o valor de quantidade_cebolas muliplicado pelo 
#preco_cebola, da seguinte forma:
#“Você irá pagar” + resultado da multiplicação + “pelas cebolas.”

def askQuant() :
    return int(input("Quantidade de cebolas : "))
def askPreco():
    return float(input("Preço das cebolas: "))

print("__________________________________")
valor = askQuant()
desconto = askPreco()
total = valor * desconto

print("\nVocê irá pagar", "R$",total, "pelas cebolas") 
print("__________________________________\n")


#2.2 Faça um comentário em português em cada linha do programa 
#abaixo explicando para você mesmo que elas fazem. Execute o 
#programa abaixo, corrigindo os erros.

print("__________________________________") 
print ("2.2 cebola.py") 

cebolas = 300 #definindo a variável cebolas
cebolas_na_caixa = 120 #definindo a variável cebolas_na_caixa
espaco_caixa = 5 #definindo a variável espaco_caixa
caixas = 60 #definindo a variável caixas

#calcula a quantidade de cebolas fora da caixa fazendo a operação 
#cebolas - cebolas_na_caixa e aplicando dentro da variável cebolas_fora_da_caixa 
cebolas_fora_da_caixa = cebolas - cebolas_na_caixa 
#calcula a quantidade de caixas vazias fazendo a operação 
#caixas - (cebolas_na_caixa/espaco_caixa) e aplicando dentro da variável caixas_vazias 
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
#calcula a quantidade de caixas necessarias fazendo a operação 
#cebolas_fora_da_caixa / espaco_caixa e aplicando dentro da variável caixas_necessarias
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print ("Existem", cebolas_na_caixa, "cebolas encaixotadas") #printa quantas cebolas encaixotadas
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa") #printa quantas cebolas sem caixa
print ("Em cada caixa cabem", espaco_caixa, "cebolas") #printa quantas cebolas
print ("Ainda temos,", caixas_vazias, "caixas vazias") #printa quantas caixas vazias
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
#printa quantas caixas precisam para empacotar todas as cebolas
print("__________________________________\n")


#Ao executar o programa corretamente você deve ver a seguinte 
#saída:

#$ python3 cebola.py
#Existem 120 cebolas encaixotadas
#Existem 180 cebolas sem caixa
#Em cada caixa cabem 5 cebolas
#Ainda temos, 36 caixas vazias
#Então, precisamos de 36 caixas para empacotar todas as cebolas

#2.3 Brinque com os valores das variáveis do programa 2.2 e veja o 
#que acontece com a saída do seu programa. 

def askCebolas():
    return int(input("Quantidade de cebolas : "))
def askCebolas_na_caixa():
    return int(input("Quantidade de cebolas na caixa : "))
def askEspaco_caixa():
    return int(input("Espaco na caixa : "))
def askCaixas():
    return int(input("Quantidade de caixas : "))

print("__________________________________")
print ("2.3 cebola.py")

cebolas = askCebolas()
cebolas_na_caixa = askCebolas_na_caixa()
espaco_caixa = askEspaco_caixa()
caixas = askCaixas()

cebolas_fora_da_caixa = cebolas - cebolas_na_caixa
caixas_vazias = caixas - (cebolas_na_caixa/espaco_caixa)
caixas_necessarias = cebolas_fora_da_caixa / espaco_caixa

print ("\nExistem", cebolas_na_caixa, "cebolas encaixotadas")
print ("Existem", cebolas_fora_da_caixa, "cebolas sem caixa")
print ("Em cada caixa cabem", espaco_caixa, "cebolas")
print ("Ainda temos,", caixas_vazias, "caixas vazias")
print ("Então, precisamos de", caixas_necessarias, "caixas para empacotar todas as cebolas")
print("__________________________________\n")


