#10.1 Use o loop for para imprimir os números de 1 a 20

i = 0
while (i != 20):
	i = i+1
	print(i)
	pass
print("\n\n\n----------------------\n\n\n")

#10.2 Faça uma lista de de 1 a um milhão e depois imprima
#cada um dos valores da lista.

lista = list(range(1, 1000000+1))
print(lista)
print("\n\n\n----------------------\n\n\n")

#10.3 Faça uma lista de de 1 a um milhão e use as funções
#min() e max() para grantir que sua lista realmente começa
#em 1 e termina em um milhão. Depois utilize a função
#sum() para imprimir a soma dos valores da lista.

lista = list(range(1, 1000000+1))
print(lista)
print("Mínimo:",min(lista), "\nMáximo:",max(lista), "\nSoma:",sum(lista))
print("\n\n\n----------------------\n\n\n")


#10.4 Use o terceiro parâmetro de função range para fazer
#uma lista com os números impares de 1 a 20. Imprima cada
#elemento da lista separadamente.

i = list(range(1,21,2))
print(i)
print("\n\n\n----------------------\n\n\n")

#10.5 Faça uma lista com todos os números múltiplos de 3,
#no intervalo de 3 a 1000. Imprima cada valor
#separadamente.

lista = list(range(3,1001,3))
print(lista)
print("\n\n\n----------------------\n\n\n")

#10.6 Um número elevado a terceira potência e chamado de
#cubo. Faça uma lista com todos os cubos entre 1 e 100,
#imprima cada valor separadamente

i=0
for x in range(1,101):
	i = i+1
	o = i**3
	print(o)
	pass
print("\n\n\n----------------------\n\n\n")


#10.7 Faça compreensão de lista para gerar a lista do
#exercício 10.6

lista = []
i=0
for x in range(1,101):
	i = i+1
	o = i**3
	lista.append(o)
	pass
print(lista)
print("\n\n\n----------------------\n\n\n")



