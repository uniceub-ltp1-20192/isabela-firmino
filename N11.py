#11.1   Slices:  Escolha   algum   dos   programas   que   você   fez   sobre
#listas, e adicione algumas linhas ao final do programa para que
#ele faça o seguinte:

print("\n----------------------\n")
lista = list(range(1,12))
print(lista)


#• Imprima   a   seguinte   mensagem:   “Os   primeiros   3   itens   da
#lista   são:   ”.   E   use   um   slice   para   imprimir   os   três
#primeiros itens da lista do programa escolhido.

primeiro_itens = lista[:3]
print("Os primeiros iteins da lista são:", primeiro_itens)

#• Imprima   a   seguinte   mensagem:   “Os   últimos   3   itens   da
#lista   são:   ”.   E   use   um   slice   para   imprimir   os   três
#últimos itens da lista do programa escolhido.

ultimos_itens = lista[-3:]
print("Os últimos iteins da lista são:", ultimos_itens)

#• Imprima   a   seguinte   mensagem:   “Os   3   itens   no   meio   da
#lista são: ”. E use um slice para imprimir os três itens
#no meio da lista do programa escolhido.

meio_itens = lista[4:7]
print("Os 3 itens no meio da lista são:", meio_itens)

#11.2  Crie uma lista de sabores de pizza e armazene em pizza_hut.

pizza_hut = ['Mussarela', 'Calabresa', 'Portuguesa', 'Frango com catupiry']

#Faça   uma   cópia   da   lista   de   pizzas   e   armazene   em
#pizza_hot_paraguai. E faça o que se pede:

pizza_hot_paraguai = pizza_hut[:]

#• Adiciona uma pizza em pizza_hut.

pizza_hut.append('Marguerita')

#• Adicione uma pizza diferente em  pizza_hot_paraguai.

pizza_hot_paraguai.append('Vegetariana')

#• Demonstre   que   a   pizza   hut   original   é   diferente   da
#cópia paraguaia!

print("Pizza Hut original:",pizza_hut, "\nCópia paraguaia:",pizza_hot_paraguai)

#11.3 Quero loops, mais loops! Todos os códigos nessa nota de aula
#imprimiram as listas sem o uso de loops. Escolha um código dessa
#nota e imprima todos os itens como um cardápio. Use um loop apra
#cada lista.

print("\n----------------------\n")
print("CARDÁPIO PIZZA HUT")
qnt = len(pizza_hut)
preco = 20
for x in range(0,qnt):
	preco += 4
	print(pizza_hut[x], ".................... R$",preco,",00")
	pass
print("\n----------------------\n")
print("CARDÁPIO PIZZA HOT PARAGUAIANA")
qtn = len(pizza_hot_paraguai)
preso = 15
for y in range(0,qtn):
	preso += 3
	print(pizza_hot_paraguai[y], ".................... R$",preso,",00")
	pass


